package sportify.dto;

import lombok.Data;

import java.util.List;

@Data
public class UserDTO {

    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String confirmPassword;
    private Long roleId;
    private List<Long> privilegesId;


}
