package sportify.dto;

import lombok.Data;

import java.util.List;

@Data
public class RoleDTO {

    private Long roleId;
    private List<PrivilegeDTO> privileges;
}
