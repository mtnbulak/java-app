package sportify.dto;

import lombok.Data;

@Data
public class PrivilegeDTO {

    private Long id;
    private String name;
}
