package sportify.users;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;


import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {

    UserEntity findByEmail(String username);

    //@Query("SELECT u FROM users u ")
    //List<UserEntity> findUsersByPage(@Param("page") Integer page);
}
