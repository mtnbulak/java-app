package sportify.users;


import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface PrivilegeRepository extends JpaRepository<Privilege, Long> {


}

