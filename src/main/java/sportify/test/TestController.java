package sportify.test;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import sportify.users.Role;
import sportify.users.RoleRepository;
import sportify.users.UserEntity;
import sportify.users.UserRepository;


import java.util.List;

@CrossOrigin(origins = "http://sportify.web.app.com")
@RestController
@RequestMapping("/test")
public class TestController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @GetMapping("/users")
    public ResponseEntity<List<UserEntity>> signupUser() {



        return ResponseEntity.ok(userRepository.findAll());
    }

    @GetMapping("/roles")
    public ResponseEntity<List<Role>> role() {



        return ResponseEntity.ok(roleRepository.findAll());
    }


    @GetMapping("/hello")
    public String hello() {



        return "hello world";
    }





    @GetMapping("/register")
    public ResponseEntity<UserEntity> register( ) {

        //convert DTO to an entity
        UserEntity userEntity = new UserEntity();
        userEntity.setPassword("123456");
        userEntity.setEmail("test@gmail.com");
        userEntity.setEnabled(true);
        userEntity.setAccountNonExpired(true);
        userEntity.setAccountNonLocked(true);
        userEntity.setCredentialsNonExpired(true);
        userEntity.setTokenExpired(false);
        userEntity.setFirstName("firstname");
        userEntity.setLastName("lastname");


        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encodedPassword = passwordEncoder.encode(userEntity.getPassword());
        userEntity.setPassword(encodedPassword);

        Role role = new Role();
        role.setId(1L);
        userEntity.getRoles().add(role);
        // userEntity.setEmail(username);

        userRepository.save(userEntity);

        return ResponseEntity.ok(userEntity);
    }

}
