package sportify.enums;

public enum City {
    NEW_YORK_CITY("New York City", "New York", "Bronx", "Kings", "Queens", "Richmond", "Westchester"),
    LOS_ANGELES("Los Angeles", "California", "Los Angeles", "Orange", "Riverside", "San Bernardino", "Ventura"),
    CHICAGO("Chicago", "Illinois", "Cook", "DuPage", "Kane", "Lake", "Will"),
    SAN_FRANCISCO("San Francisco", "California", "San Francisco", "San Mateo", "Marin", "Santa Clara", "Alameda"),
    HOUSTON("Houston", "Texas", "Harris", "Fort Bend", "Montgomery", "Galveston", "Brazoria");

    private final String city;
    private final String state;
    private final String[] counties;

    City(String city, String state, String... counties) {
        this.city = city;
        this.state = state;
        this.counties = counties;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public String[] getCounties() {
        return counties;
    }
}

