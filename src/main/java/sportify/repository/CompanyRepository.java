package sportify.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import sportify.entity.tables.CompanyEntity;

import java.util.List;

@Repository
public interface CompanyRepository extends JpaRepository<CompanyEntity, Long> {

   // List<CompanyEntity> findByCompanyId(Integer type);

    @Query("SELECT c FROM CompanyEntity c WHERE c.id = :fieldValue")
    List<CompanyEntity> findByCompanyId(@Param("fieldValue") Integer fieldValue);

}
