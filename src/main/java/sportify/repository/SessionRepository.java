package sportify.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sportify.entity.tables.SessionEntity;

@Repository
public interface SessionRepository extends JpaRepository<SessionEntity, Long> {
}
