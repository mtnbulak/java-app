package sportify.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sportify.entity.tables.BlogEntity;
import sportify.entity.tables.ProductEntity;


@Repository
public interface ProductRepository extends JpaRepository<ProductEntity, Long> {
}

