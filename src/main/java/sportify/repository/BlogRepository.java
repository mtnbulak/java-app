package sportify.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sportify.entity.tables.BlogEntity;
import sportify.users.Role;

@Repository
public interface BlogRepository extends JpaRepository<BlogEntity, Long> {
}
