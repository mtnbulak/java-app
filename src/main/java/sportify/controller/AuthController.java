package sportify.controller;


import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;


import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.ErrorResponse;
import org.springframework.web.bind.annotation.*;
import sportify.dto.UserDTO;
import sportify.entity.tables.SessionEntity;
import sportify.repository.SessionRepository;
import sportify.users.*;

import java.time.LocalDateTime;

//@CrossOrigin(maxAge = 3600)
//@CrossOrigin("http://localhost:4200")
//@CrossOrigin(origins = "http://sportify.web.app.com")
@RestController
@RequestMapping("/auth")
public class AuthController {



    @Autowired
    UserRepository userRepository;
    @Autowired
    SessionRepository sessionRepository;
    @Autowired
    RoleRepository roleRepository;


    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
     ModelMapper modelMapper;







    @PostMapping("/login")
    public ResponseEntity<Object> login(@RequestBody UserDTO userDto, ServletRequest servletRequest, ServletResponse servletResponse) {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession sessions =  request.getSession();

        // Access information related to the request like device, browser, IP address, etc.
        String device = request.getHeader("User-Agent");
        String browser = request.getHeader("User-Agent");
        String ipAddress = request.getRemoteAddr();

        // Store the information in the session information table
        SessionEntity session = new SessionEntity();
        session.setSessionId(sessions.getId());
        session.setDeviceInfo(device);
        session.setBrowserInfo(browser);
        session.setIpAddress(ipAddress);
        session.setStartTime(LocalDateTime.now());


       // sessionRepository.save(session);
        MyUserDetails userDetails = null;
        try {
        
            UsernamePasswordAuthenticationToken authReq
                    = new UsernamePasswordAuthenticationToken(userDto.getEmail(), userDto.getPassword());
            Authentication auth = authenticationManager.authenticate(authReq);
            SecurityContext sc = SecurityContextHolder.getContext();
            sc.setAuthentication(auth);
        
              userDetails = (MyUserDetails) auth.getPrincipal();
        
            session.setUser(userDetails.getUserEntity());
            sessionRepository.save(session);

        }catch(BadCredentialsException ex){
           // ErrorResponse error = new ErrorResponse("Invalid username or password");
            return new ResponseEntity<>("error", HttpStatus.UNAUTHORIZED);
           // return ResponseEntity.badRequest().body("Password and Confirm Password must match and cannot be blank");
        }catch(Exception ex){
            ex.printStackTrace();
        }

        return ResponseEntity.ok(userDetails);
    }

    @PostMapping("/register")
    public ResponseEntity<Object> register(@RequestBody UserDTO userDto) {

        if(userDto.getPassword().isBlank() || !userDto.getPassword().equals(userDto.getConfirmPassword()) ){

            return ResponseEntity.badRequest().body("Password and Confirm Password must match and cannot be blank");
        }

        //convert DTO to an entity
        UserEntity userEntity = modelMapper.map(userDto, UserEntity.class);
        userEntity.setEnabled(true);
        userEntity.setAccountNonExpired(true);
        userEntity.setAccountNonLocked(true);
        userEntity.setCredentialsNonExpired(true);
        userEntity.setTokenExpired(false);
       // userEntity.setFirstName("firstname");
       // userEntity.setLastName("lastname");


        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encodedPassword = passwordEncoder.encode(userEntity.getPassword());
        userEntity.setPassword(encodedPassword);

        Role role = roleRepository.findById(1L).get();

        userEntity.getRoles().add(role);
        // userEntity.setEmail(username);

        userRepository.save(userEntity);

        return ResponseEntity.ok(userDto);
    }

}
