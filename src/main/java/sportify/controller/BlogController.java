package sportify.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sportify.dto.UserDTO;
import sportify.entity.tables.BlogEntity;
import sportify.repository.BlogRepository;
import sportify.users.Role;
import sportify.users.UserRepository;

import java.util.List;

@RestController
@RequestMapping("/")
public class BlogController {


    @Autowired
    BlogRepository blogRepository;

    @GetMapping(value = "/")
    public String index() {
        return "hello world";
    }

    @GetMapping("/all")
    public ResponseEntity<List<BlogEntity>> blogs( ) {



        return ResponseEntity.ok(blogRepository.findAll());
    }



    @GetMapping("/{id}")
    public ResponseEntity<BlogEntity> blog(@PathVariable Long id ) {


        BlogEntity gg = blogRepository.findById(id).get();
        return ResponseEntity.ok(gg);
    }

    @PostMapping("")
    public ResponseEntity<BlogEntity> insert(@PathVariable Long id ) {


        BlogEntity gg = blogRepository.findById(id).get();
        return ResponseEntity.ok(gg);
    }

    @PutMapping("")
    public ResponseEntity<BlogEntity> update(@PathVariable Long id ) {


        BlogEntity gg = blogRepository.findById(id).get();
        return ResponseEntity.ok(gg);
    }


    @DeleteMapping("")
    public ResponseEntity<BlogEntity> delete(@PathVariable Long id ) {


        BlogEntity gg = blogRepository.findById(id).get();
        return ResponseEntity.ok(gg);
    }

}
