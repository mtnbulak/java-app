package sportify.controller;

import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sportify.dto.UserDTO;
import sportify.entity.tables.ProductEntity;
import sportify.repository.OrderRepository;
import sportify.repository.ProductRepository;
import sportify.users.Role;
import sportify.users.UserEntity;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/product")
public class ProductController {


    @Autowired
    ProductRepository productRepository;


    @GetMapping("")
    public ResponseEntity<Page<ProductEntity>> all(@RequestParam(defaultValue = "0") int page,
                                                @RequestParam(defaultValue = "10") int size) {

        //Page<User> users = userRepository.getUsersPerPage(page, size);
        Pageable pageable = PageRequest.of(page, size);
        Page<ProductEntity> res = productRepository.findAll(pageable);




        return ResponseEntity.ok(res);
    }

    @PostMapping
    public ProductEntity create(HttpServletRequest request) {

        String productname = request.getParameter("productname");
        String password = request.getParameter("productprice");

        ProductEntity entity = new ProductEntity();
        entity.setName(productname);


        return productRepository.save(entity);
    }

    @GetMapping("/{id}")
    public ProductEntity getById(@PathVariable Long id) throws Exception {
        return productRepository.findById(id)
                .orElseThrow(() -> new  Exception("User not found"));
    }

    @PutMapping("/{id}")
    public ProductEntity updateUser(@PathVariable Long id, HttpServletRequest request) throws Exception {
        ProductEntity entity = productRepository.findById(id)
                .orElseThrow(() -> new  Exception("User not found"));

        entity.setName(request.getParameter("productname"));


        ProductEntity updatedUser = productRepository.save(entity);
        return updatedUser;
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable Long id) throws Exception {
        ProductEntity user = productRepository.findById(id)
                .orElseThrow(() -> new  Exception("User not found"));

        productRepository.delete(user);

        return ResponseEntity.ok().build();
    }





}
