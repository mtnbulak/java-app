package sportify.controller;

import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import sportify.dto.RoleDTO;
import sportify.dto.UserDTO;
import sportify.entity.tables.CompanyEntity;
import sportify.users.Role;
import sportify.users.RoleRepository;
import sportify.users.UserEntity;
import sportify.users.UserRepository;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

//@PreAuthorize("hasRole('ROLE_USER')")
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
     ModelMapper modelMapper;




    @GetMapping("")
    public ResponseEntity<Page<UserEntity> > all(@RequestParam(defaultValue = "0") int page,
                                                @RequestParam(defaultValue = "10") int size) {

        //Page<User> users = userRepository.getUsersPerPage(page, size);
        Pageable pageable = PageRequest.of(page, size);
        Page<UserEntity> res = userRepository.findAll(pageable);




        return ResponseEntity.ok(res);
    }


    @GetMapping("/{id}/roles")
    public ResponseEntity<List<Role>> roles(@PathVariable Long userId) {

          ;



        return ResponseEntity.ok(roleRepository.findByUsers(userId));
    }



        @PostMapping
        public UserEntity createUser(@RequestBody UserDTO userDto) {

            UserEntity userEntity = modelMapper.map(userDto, UserEntity.class);
            Role role = new Role();
            role.setId(userDto.getRoleId());
            List<Role> roles = new ArrayList<>();
            roles.add(role);

            userEntity.setRoles(roles);


            return userRepository.save(userEntity);
        }

        @GetMapping("/{id}")
        public UserEntity getUserById(@PathVariable Long id) throws Exception {
            return userRepository.findById(id)
                    .orElseThrow(() -> new  Exception("User not found"));
        }

        @PutMapping("/{id}")
        public UserEntity updateUser(@PathVariable Long id, @RequestBody UserDTO userDetails) throws Exception {
            UserEntity user = userRepository.findById(id)
                    .orElseThrow(() -> new  Exception("User not found"));

            user.setLastName(userDetails.getLastName());
            user.setEmail(userDetails.getLastName());

            UserEntity updatedUser = userRepository.save(user);
            return updatedUser;
        }

        @DeleteMapping("/{id}")
        public ResponseEntity<?> deleteUser(@PathVariable Long id) throws Exception {
            UserEntity user = userRepository.findById(id)
                    .orElseThrow(() -> new  Exception("User not found"));

            userRepository.delete(user);

            return ResponseEntity.ok().build();
        }





}
