package sportify.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sportify.dto.UserDTO;
import sportify.entity.tables.CompanyEntity;
import sportify.repository.CompanyRepository;
import sportify.users.UserEntity;
import sportify.users.UserRepository;

import java.util.List;

@RestController
@RequestMapping("/company")
public class CompanyController {

    @Autowired
    CompanyRepository companyRepository;


    @GetMapping("/")
    public ResponseEntity<List<CompanyEntity>> companytype(@RequestBody Integer categoryId ) {



        return ResponseEntity.ok(companyRepository.findAll());
    }


    @GetMapping("/all")
    public ResponseEntity<List<CompanyEntity>> all( ) {



        return ResponseEntity.ok(companyRepository.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<CompanyEntity> select(@PathVariable Long id ) {



        return ResponseEntity.ok(companyRepository.findById(id).get());
    }

    @PutMapping("/{id}")
    public ResponseEntity<List<CompanyEntity>> update(@PathVariable Long id, @RequestBody UserDTO client ) {



        return ResponseEntity.ok(companyRepository.findAll());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<List<CompanyEntity>> delete( @PathVariable Long id) {



        return ResponseEntity.ok(companyRepository.findAll());
    }

    @PostMapping("/")
    public ResponseEntity<List<CompanyEntity>> create(@RequestBody UserDTO userDto) {



        return ResponseEntity.ok(companyRepository.findAll());
    }

    @PostMapping("/column")
    public ResponseEntity<List<CompanyEntity>> create( Long tableid,@RequestBody String columnname) {

        CompanyEntity model = companyRepository.findById(tableid).get();

        model.setName(columnname);

        companyRepository.save(model);

        return ResponseEntity.ok(companyRepository.findAll());
    }

}
