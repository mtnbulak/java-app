package sportify.controller;

import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sportify.dto.UserDTO;
import sportify.entity.tables.OrderEntity;
import sportify.entity.tables.ProductEntity;
import sportify.repository.OrderRepository;
import sportify.users.Role;
import sportify.users.UserEntity;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/order")
public class OrderController {



    @Autowired
    OrderRepository orderRepository;


    @GetMapping("")
    public ResponseEntity<Page<OrderEntity>> all(@RequestParam(defaultValue = "0") int page,
                                                 @RequestParam(defaultValue = "10") int size) {

        //Page<User> users = userRepository.getUsersPerPage(page, size);
        Pageable pageable = PageRequest.of(page, size);
        Page<OrderEntity> res = orderRepository.findAll(pageable);




        return ResponseEntity.ok(res);
    }




    @PostMapping
    public OrderEntity create(HttpServletRequest request) {


        Integer orderprice = Integer.valueOf(request.getParameter("orderprice"));

        OrderEntity entity = new OrderEntity();
        entity.setPrice(orderprice);


        return orderRepository.save(entity);
    }

    @GetMapping("/{id}")
    public OrderEntity getUserById(@PathVariable Long id) throws Exception {
        return orderRepository.findById(id)
                .orElseThrow(() -> new  Exception("User not found"));
    }

    @PutMapping("/{id}")
    public OrderEntity updateUser(@PathVariable Long id, HttpServletRequest request) throws Exception {
        OrderEntity entity = orderRepository.findById(id)
                .orElseThrow(() -> new  Exception("User not found"));


        entity.setPrice(Integer.valueOf(request.getParameter("orderprice")));

        OrderEntity updatedUser = orderRepository.save(entity);
        return updatedUser;
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable Long id) throws Exception {
        OrderEntity user = orderRepository.findById(id)
                .orElseThrow(() -> new  Exception("User not found"));

        orderRepository.delete(user);

        return ResponseEntity.ok().build();
    }





}
