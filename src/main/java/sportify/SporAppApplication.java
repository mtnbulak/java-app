package sportify;

import jakarta.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import sportify.users.Role;
import sportify.users.RoleRepository;


@SpringBootApplication
public class SporAppApplication {

	@Autowired
	RoleRepository roleRepository;

	public static void main(String[] args) {
		SpringApplication.run(SporAppApplication.class, args);
	}



	@PostConstruct
	public void postConstruct() {

	}

	public void init222() {

		Role role = new Role();

		//role.setId(1L);
		role.setName("ROLE_USER");

		roleRepository.save(role);

	}

}
