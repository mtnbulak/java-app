package sportify.entity.tables;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@Entity
@Table(name="orders")
public class OrderEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // stok, pending, available,
    @Column(name="status",nullable = false)
    Integer status;

    @Column(name="companyid",nullable = false)
    Long companyId;

    @Column(name="productid",nullable = false)
    Long productId;

    @Column(name="userid",nullable = false)
    Long userId;

    @Column(columnDefinition = "integer default 0")
    Integer price;

   // String destination;
}
