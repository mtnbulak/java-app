package sportify.entity.tables;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@Entity
@Table(name="company")
public class CompanyEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // company Type (e.g. "spor salonu", "fitness", "yüzme")
   // @Column(name="companytype",nullable = false)
    private Integer categoryId;

    @Column(nullable = false)
    private String name;
}
