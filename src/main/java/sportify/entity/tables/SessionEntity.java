package sportify.entity.tables;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import sportify.users.UserEntity;

import java.time.LocalDateTime;
import java.util.Date;
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "sessions")
public class SessionEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "session_id", nullable = false, unique = true)
    private String sessionId;


    @Column(name = "location_info", nullable = true)
    private String locationInfo;

    @Column(name = "device_info", nullable = false)
    private String deviceInfo;

    @Column(name = "browser_info", nullable = false)
    private String browserInfo;

    @Column(name = "ip_address", nullable = false)
    private String ipAddress;

    @Column(name = "start_time", nullable = false)
    private LocalDateTime startTime;

    @Column(name = "end_time", nullable = true)
    private LocalDateTime endTime;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = true)
    private UserEntity user;





    // Getters and Setters
}

