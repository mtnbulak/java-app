package sportify.entity.tables;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@Entity
@Table(name="adress")
public class AdressEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


// il ilçe, mahalle, cadde, sokak, no, kapı kat daire, postakodu
    @Column(nullable = false)
    private Integer province;

    // ilçe
    @Column(nullable = false)
    private String district;

    // mahalle
    @Column(nullable = false)
    private String neighborhood;

    // adres
    @Column(nullable = false)
    String address;



    @CreatedDate
    @Column(name = "created_at",nullable = false, updatable = false)
    private LocalDateTime createdAt;

    @LastModifiedDate
    @Column(name = "updated_at",nullable = false)
    private LocalDateTime updatedAt;



}
