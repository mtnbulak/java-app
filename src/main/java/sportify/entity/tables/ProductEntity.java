package sportify.entity.tables;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.databind.node.ObjectNode;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@Entity
@Table(name="products")
public class ProductEntity {

    // ürün bilgilerinin tutulduğu table

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(columnDefinition = "varchar(255) default 'John Snow'")
    String name;
    @Column(columnDefinition = "integer default 25")
    String price;
    // stok, pending, available,
    String status;

    // product Type (e.g. içecek, yemek,)
    @Column(name="producttype",nullable = false)
    private Long productType;

  //  @JsonSubTypes.Type(type = "jsonb")
  //  @Column(columnDefinition = "jsonb")
   // private ObjectNode property;

   // @Column(columnDefinition = "longtext")
    //private String property;

    // both
    @Lob
    private String property;

    @CreatedDate
    @Column(name = "created_at",nullable = false, updatable = false)
    private LocalDateTime createdAt;

    @LastModifiedDate
    @Column(name = "updated_at",nullable = false)
    private LocalDateTime updatedAt;
}
