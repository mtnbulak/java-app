package sportify.entity.mappings;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@Entity
@Table(name="user_adress")
public class UserAddressEntity {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // Address Type (e.g. "Home", "Work", "Company")
    @Column(nullable = false)
    private String addressType;

    @Column(name="userid",nullable = false)
    private Long userId;

    @Column(name="addressid",nullable = false)
    private Long addressId;


}
