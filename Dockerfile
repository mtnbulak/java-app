FROM openjdk:18-jdk AS builder

WORKDIR /app

COPY .mvn/ .mvn
ADD ./pom.xml mvnw ./ 
ADD ./src src/

RUN sed -i 's/\r$//' mvnw


RUN ./mvnw package -DskipTests


From builder as runner

WORKDIR /app

COPY --from=builder /app/target/*.jar my-app.jar

EXPOSE 8080

CMD ["java", "-jar", "my-app.jar"]